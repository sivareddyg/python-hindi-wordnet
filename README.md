# Python Hindi Wordnet


## Usage

Run the command "make" to create python pickle files. Replace database files with your desired Hindi Wordnet version downloadable from http://www.cfilt.iitb.ac.in/wordnet/webhwn/

### Refer to [README.md](https://bitbucket.org/sivareddyg/python-hindi-wordnet/src/master/hindi_wordnet_python/) file in hindi_wordnet_python directory on how to use these pickle files.


## Stemmer

If you just want to use hindi word stemmer, use the following command

>   java -jar jython-standalone-2.5.3.jar getStem.py

It asks for a word input, and outputs its stem

## Citation

Please cite http://sivareddy.in/downloads wherever required.

## Contributor:

Siva Reddy
http://sivareddy.in/downloads